package com.cencosud.sim.constants;

import com.cencosud.sim.util.LeerProperties;

public class Constants {
	
	
	
	private Constants() {
	}
	public static final int CERO = 0;
	public static final int PENDIENTE= 1;
	public static final int VALIDADO = 2;
	public static final int VALIDADO_ERROR = 3;
	public static final int PROCESADO = 4;
	public static final int PROCESADO_ERROR = 5;
	public static final int GENERADO = 6;
	public static final int DISTRIBUIDO = 7;
	public static final int SIN_PLANTILLA = 11;
	public static final int SIN_ERROR = 12;
	public static final int ERROR_PROCESO = 13;
	public static final int EMPRESA_CARGA = 1;
	public static final String BTN_VALIDAR = LeerProperties.readStaticText("boton.valdar");
	public static final String BTN_PROCESAR = LeerProperties.readStaticText("boton.procesar");
	public static final String BTN_RE_VALIDAR = LeerProperties.readStaticText("boton.revalidar");
	public static final String BTN_GENERAR_SALIDA = LeerProperties.readStaticText("boton.generarsalida");
	public static final String BTN_DISTRIBUIR = LeerProperties.readStaticText("boton.distribuir");
	public static final String HORA_INICIO = " 00:00:00";
	public static final String HORA_FIN = " 23:59:59";
	public static final String FECHA_FRONT = "dd/MM/yyyy";
	public static final String FECHA_BACK = "dd-MM-yyyy";
	public static final int EMPRESA_CORREO_ELECTRONICO = 10;
	public static final String STATIC_TEXT_PROPERTIES_FILE = "global.properties";
	public static final String EXCEL_RUTA_RESPUESTA = LeerProperties.readStaticText("excel.ruta.respuesta");
	
	/*
	 * Datos de conexion para la carpeta compartida
	 */
	public static final String SERVER = LeerProperties.readStaticText("server");
	public static final String USERNAMESERVER = LeerProperties.readStaticText("usernameServer");
	public static final String PASSWORD = LeerProperties.readStaticText("password");
	
	/*
	 * Al parecer, cuando instalaron wkhtmltopdf en el servidor no configuraron
	 * la variable de entorno para ejecutarlo en cualquier parte del sistema.
	 * Se deja la ruta del ejecutable, pero lo optimo seria realizar una correcta configuracion.
	 */
	public static final String WKHTMLTOPDF = LeerProperties.readStaticText("wkhtmltopdf");
	
	/*
	 * Se define ruta de salida para la generacion de polizas
	 */
	public static final String RUTA_ESCRITURA_ARCHIVOS = LeerProperties.readStaticText("salida");
	public static final String RUTA_FILE_SHARE = LeerProperties.readStaticText("rutaFileShare");
	
	public static final String EXCEL_COLUMNA_ESTADO = LeerProperties.readStaticText("excel.columna.estado");
	public static final String EXCEL_MAIL_NOENVIADO = LeerProperties.readStaticText("excel.mail.noenviado");
	public static final String EXCEL_MAIL_ENVIADO = LeerProperties.readStaticText("excel.mail.enviado");
	public static final String EXCEL_CORREO_PROCESO = LeerProperties.readStaticText("excel.correo.proceso");
	
	public static final String EXCEL_MENSAJE_ERROR_FORMATO = LeerProperties.readStaticText("excel.mensaje.error.formato");
	public static final String EXCEL_MENSAJE_ERROR_INCONSISTENTE = LeerProperties.readStaticText("excel.mensaje.error.inconsistencia");
	public static final String EXCEL_MENSAJE_PREGUNTA_NUEVOID = LeerProperties.readStaticText("excel.mensaje.pregunta.nuevoid");
	public static final String EXCEL_MENSAJE_PREGUNTA_INGRESAR_NUEVOID = LeerProperties.readStaticText("excel.mensaje.pregunta.ingresar.nuevoid");
	public static final int EXCEL_ESTADO_PREGUNTA_NUEVOID = Integer.parseInt(LeerProperties.readStaticText("excel.estado.pregunta.nuevoid"));
	public static final int EXCEL_ESTADO_PREGUNTA_INGRESAR_NUEVOID = Integer.parseInt(LeerProperties.readStaticText("excel.estado.pregunta.ingresar.nuevoid"));
	public static final int EXCEL_ESTADO_ERROR_FORMATO = Integer.parseInt(LeerProperties.readStaticText("excel.estado.error.formato"));
	public static final String EXCEL_ESTADO_PREGUNTA_RESPUESTA_NUEVOID = LeerProperties.readStaticText("excel.mensaje.respuesta.nuevoId");
	public static final String ENCODING = "iso-8859-1";
	public static final String EXCEL_FORMATO_CORREO_ELECTRONICO = LeerProperties.readStaticText("excel.formato.correo.electronico");
	public static final String EXCEL_FORMATO_CORREO_TRADICIONAL = LeerProperties.readStaticText("excel.formato.correo.tradicional");
	public static final String SOCKET = "/tema/barras";
	public static final String BARRA_DIAGONAL = "/";
	
	//Encabezado correo tradicional
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_ENVIO = LeerProperties.readStaticText("excel.encabezado.tradicional.envio");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_REFERENCIA = LeerProperties.readStaticText("excel.encabezado.tradicional.referencia");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_ESTADO = LeerProperties.readStaticText("excel.encabezado.tradicional.estado");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_INCIDENCIA = LeerProperties.readStaticText("excel.encabezado.tradicional.incidencia");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_FECHAADMISION = LeerProperties.readStaticText("excel.encabezado.tradicional.fechaadmision");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_FECHAENTREGA = LeerProperties.readStaticText("excel.encabezado.tradicional.fechaentrega");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_NOMBRE = LeerProperties.readStaticText("excel.encabezado.tradicional.nombre");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_DIRECCION = LeerProperties.readStaticText("excel.encabezado.tradicional.direccion");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_COMUNA = LeerProperties.readStaticText("excel.encabezado.tradicional.comuna");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_OT = LeerProperties.readStaticText("excel.encabezado.tradicional.ot");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_IDCLIENTE = LeerProperties.readStaticText("excel.encabezado.tradicional.idcliente");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_PROYECTO = LeerProperties.readStaticText("excel.encabezado.tradicional.proyecto");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_COR = LeerProperties.readStaticText("excel.encabezado.tradicional.cor");
	public static final String EXCEL_ENCABEZADO_TRADICIONAL_SERVICIO = LeerProperties.readStaticText("excel.encabezado.tradicional.servicio");
	
	//Encabezado correo electronico
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_FECHA_ENVIO = LeerProperties.readStaticText("excel.encabezado.electronico.fecha.envio");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_HORA_ENVIO = LeerProperties.readStaticText("excel.encabezado.electronico.hora.envio");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_ENVIADO = LeerProperties.readStaticText("excel.encabezado.electronico.enviado");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_CORREO = LeerProperties.readStaticText("excel.encabezado.electronico.correo");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_CODIGO_PDF = LeerProperties.readStaticText("excel.encabezado.electronico.codigo.pdf");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_SMTP = LeerProperties.readStaticText("excel.encabezado.electronico.smtp");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_ESTADO = LeerProperties.readStaticText("excel.encabezado.electronico.estado");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_CODIGO_BARRA = LeerProperties.readStaticText("excel.encabezado.electronico.codigo.barra");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_TIPO_REBOTE = LeerProperties.readStaticText("excel.encabezado.electronico.tipo.rebote");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_REGLA_REBOTE = LeerProperties.readStaticText("excel.encabezado.electronico.regla.rebote");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_FECHA_REBOTE = LeerProperties.readStaticText("excel.encabezado.electronico.fecha.rebote");
	public static final String EXCEL_ENCABEZADO_ELECTRONICO_FECHA_RECEPCION = LeerProperties.readStaticText("excel.encabezado.electronico.fecha.recepcion");
	
	public static final String EXCEL_TRADICIONAL_ENTREGADO = LeerProperties.readStaticText("excel.tradicional.entregado");
	public static final String EXTENSION_EXCEL_XLS = "xls";
	public static final String EXTENSION_EXCEL_XLSX = "xlsx";
	
}
