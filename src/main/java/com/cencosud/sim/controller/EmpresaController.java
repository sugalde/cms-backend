package com.cencosud.sim.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.model.EmpresaModel;
import com.cencosud.sim.model.request.EmpresaRequest;
import com.cencosud.sim.service.EmpresaService;

@RestController
@RequestMapping("/rest")
public class EmpresaController {
	static final Logger log = Logger.getLogger(EmpresaController.class);
	
	@Autowired
	@Qualifier("EmpresaService")
	private EmpresaService empresaService;
	
	@CrossOrigin(origins = "*")
	@PostMapping("/empresaRecepcion")
	public List<EmpresaModel> obtenerEmpresaRecepcion(@RequestBody EmpresaRequest empresaRequest) {
		log.info("INGRESA A ===> obtenerEmpresaRecepcion("+empresaRequest+")");
		return empresaService.obtenerEmpresaRecepcion(empresaRequest);
	}
}
