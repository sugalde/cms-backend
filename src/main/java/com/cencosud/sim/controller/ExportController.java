package com.cencosud.sim.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cencosud.sim.model.request.ExcelRequest;
import com.cencosud.sim.service.ExportService;
import com.cencosud.sim.util.ExcelView;

/**
 * Encargado de obtener la data procesada y generar un archivo excel que sera descargado
 * por el usuario
 * 
 */
@Controller
@ComponentScan(basePackages = {"com.cencosud.sim.util"})
public class ExportController {
	static final Logger log = Logger.getLogger(ExportController.class);
	
	
	@Autowired
	@Qualifier("ExportService")
	private ExportService exportService;


	/**
	 * Encargado de solicitar la descarga del documento Excel
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(path = "/rest/propuestaValor", method = RequestMethod.POST)
	public ModelAndView propuestaValor(Model model, @RequestBody ExcelRequest excelRequest) {
		log.info("INGRESA A ===> propuestaValor("+excelRequest+")");
		exportService.generarPropuestas(model,excelRequest);
		return new ModelAndView(new ExcelView());
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(path = "/rest/obtenerArchivoRespuesta", method = RequestMethod.POST)
	public ModelAndView obtenerArchivoRespuesta(Model model, @RequestBody ExcelRequest excelRequest) {
		log.info("INGRESA A ===> obtenerArchivoRespuesta("+excelRequest+")");
		exportService.obtenerArchivoRespuesta(model,excelRequest);
		return new ModelAndView(new ExcelView());
	}
	
}
