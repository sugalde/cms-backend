package com.cencosud.sim.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.request.HistorialRequest;
import com.cencosud.sim.service.RecepcionService;

@RestController
@RequestMapping("/rest")
public class RespuestaController {
	static final Logger log = Logger.getLogger(RespuestaController.class);
	
	@Autowired
	@Qualifier("RecepcionService")
	private RecepcionService recepcionService;
	
	@CrossOrigin(origins = "*")
	@PostMapping("/obtenerHistorial")
	public List<RecepcionModel> obtenerHistorial(@RequestBody HistorialRequest historialRequest) {
		log.info("INGRESA A ===> obtenerHistorial("+historialRequest+")");
		return recepcionService.obtenerHistorial(historialRequest);
	}
}
