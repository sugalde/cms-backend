package com.cencosud.sim.controller.intranet;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.dto.CargaManual;
import com.cencosud.sim.dto.SelectOrigen;

@RestController
public class CargaManualController implements BaseServiciosIntranet {
	
	@GetMapping("getOrigen")
	public List<SelectOrigen> select() {
		List<SelectOrigen> list = new ArrayList<SelectOrigen>();
		SelectOrigen origen;
		
		origen = new SelectOrigen();
		origen.setId(1);
		origen.setValue("VENTAS");
		list.add(origen);
		
		origen = new SelectOrigen();
		origen.setId(2);
		origen.setValue("RENUNCIA");
		list.add(origen);
		
		origen = new SelectOrigen();
		origen.setId(3);
		origen.setValue("CORTE POR EDAD");
		list.add(origen);
		
		origen = new SelectOrigen();
		origen.setId(4);
		origen.setValue("WORKFLOW");
		list.add(origen);
		
		return list;
	}
	
	@GetMapping("procesar")
	public Boolean procesar(CargaManual carga) {
		return true;
	}
}
