package com.cencosud.sim.dto;

import java.io.Serializable;

public class Accion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String url;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Accion(String nombre) {
		super();
		this.nombre = nombre;
	}
	public Accion() {
		super();
	}
	
	
}
