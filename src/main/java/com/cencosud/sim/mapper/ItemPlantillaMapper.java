package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.ItemPlantillaModel;

@Mapper
public interface ItemPlantillaMapper {

	List<ItemPlantillaModel> obtenerItemPlantillaFiltrado(@Param("plantillasAprocesar") List<ItemPlantillaModel> plantillasAprocesar);

	void guardarDatosPlantilla(@Param("plantillasAGuardar") List<ItemPlantillaModel> plantillasAGuardar);

}
