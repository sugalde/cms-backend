package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ObtenerValoresMapper {

	String obtenerItem(@Param("itemId") int itemId);

	String obtenerValorItem(@Param("sqlQuery") String sqlQuery, @Param("valorBuscado") String valorBuscado);

	List<String> obtenerPlantillas();

}
