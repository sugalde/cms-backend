package com.cencosud.sim.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.cencosud.sim.model.request.CargaPcbsRequest;

@Mapper
public interface OperacionMapper {

	int registrarErrores(@Param("error") Map<String, Integer> error);

	void quitarErrores(@Param("txId") int txId);

	int validarComuna(@Param("cargaPcbsRequest") CargaPcbsRequest cargaPcbsRequest);

	Long obtenerErrores(@Param("txId") int txId);

}
