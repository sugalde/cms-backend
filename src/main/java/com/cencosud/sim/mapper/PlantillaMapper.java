package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.PlantillaModel;
import com.cencosud.sim.model.PlantillaPropuestaModel;

@Mapper
public interface PlantillaMapper {

	List<PlantillaModel> buscarPlantillas(int txId);

	List<PlantillaPropuestaModel> buscarPlantilla(@Param("txId") int txId);

}
