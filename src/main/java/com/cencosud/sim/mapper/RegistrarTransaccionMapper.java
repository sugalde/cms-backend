package com.cencosud.sim.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RegistrarTransaccionMapper {
	int registrarTransaccion(@Param("name") String name,@Param("age") int age);
}
