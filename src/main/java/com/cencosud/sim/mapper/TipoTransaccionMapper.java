package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cencosud.sim.model.TipoTransaccionModel;

@Mapper
public interface TipoTransaccionMapper {

	List<TipoTransaccionModel> obtenerTipoTransaccion();

}
