package com.cencosud.sim.model;

import java.io.Serializable;

public class CargaPcbsModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pcbsId;
	private int pcbsCodCompania;
	private int pcbsCodNegocio;
	private int pcbsCodRamo;
	private int pcbsTpoPago;
	private int pcbsRutPropuesta;
	private String pcbsFchCotizacion;
	private String pcbsCorreo;
	private String pcbsTpoVenta;
	private int pcbsCodMail;
	private int pcbsComuna;
	private int pcbsRegion;
	private String empIdDespacho;
	private String empIdImprenta;
	private int txId;
	private int planId;
	private int pptaId;
	private String error;
	public int getPcbsId() {
		return pcbsId;
	}
	public void setPcbsId(int pcbsId) {
		this.pcbsId = pcbsId;
	}
	public int getPcbsCodCompania() {
		return pcbsCodCompania;
	}
	public void setPcbsCodCompania(int pcbsCodCompania) {
		this.pcbsCodCompania = pcbsCodCompania;
	}
	public int getPcbsCodNegocio() {
		return pcbsCodNegocio;
	}
	public void setPcbsCodNegocio(int pcbsCodNegocio) {
		this.pcbsCodNegocio = pcbsCodNegocio;
	}
	public int getPcbsCodRamo() {
		return pcbsCodRamo;
	}
	public void setPcbsCodRamo(int pcbsCodRamo) {
		this.pcbsCodRamo = pcbsCodRamo;
	}
	public int getPcbsTpoPago() {
		return pcbsTpoPago;
	}
	public void setPcbsTpoPago(int pcbsTpoPago) {
		this.pcbsTpoPago = pcbsTpoPago;
	}
	public int getPcbsRutPropuesta() {
		return pcbsRutPropuesta;
	}
	public void setPcbsRutPropuesta(int pcbsRutPropuesta) {
		this.pcbsRutPropuesta = pcbsRutPropuesta;
	}
	public String getPcbsFchCotizacion() {
		return pcbsFchCotizacion;
	}
	public void setPcbsFchCotizacion(String pcbsFchCotizacion) {
		this.pcbsFchCotizacion = pcbsFchCotizacion;
	}
	public String getPcbsCorreo() {
		return pcbsCorreo;
	}
	public void setPcbsCorreo(String pcbsCorreo) {
		this.pcbsCorreo = pcbsCorreo;
	}
	public String getPcbsTpoVenta() {
		return pcbsTpoVenta;
	}
	public void setPcbsTpoVenta(String pcbsTpoVenta) {
		this.pcbsTpoVenta = pcbsTpoVenta;
	}
	public int getPcbsCodMail() {
		return pcbsCodMail;
	}
	public void setPcbsCodMail(int pcbsCodMail) {
		this.pcbsCodMail = pcbsCodMail;
	}
	public int getPcbsComuna() {
		return pcbsComuna;
	}
	public void setPcbsComuna(int pcbsComuna) {
		this.pcbsComuna = pcbsComuna;
	}
	public int getPcbsRegion() {
		return pcbsRegion;
	}
	public void setPcbsRegion(int pcbsRegion) {
		this.pcbsRegion = pcbsRegion;
	}
	public String getEmpIdDespacho() {
		return empIdDespacho;
	}
	public void setEmpIdDespacho(String empIdDespacho) {
		this.empIdDespacho = empIdDespacho;
	}
	public String getEmpIdImprenta() {
		return empIdImprenta;
	}
	public void setEmpIdImprenta(String empIdImprenta) {
		this.empIdImprenta = empIdImprenta;
	}
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public int getPptaId() {
		return pptaId;
	}
	public void setPptaId(int pptaId) {
		this.pptaId = pptaId;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CargaPcbsModel [pcbsId=");
		builder.append(pcbsId);
		builder.append(", pcbsCodCompania=");
		builder.append(pcbsCodCompania);
		builder.append(", pcbsCodNegocio=");
		builder.append(pcbsCodNegocio);
		builder.append(", pcbsCodRamo=");
		builder.append(pcbsCodRamo);
		builder.append(", pcbsTpoPago=");
		builder.append(pcbsTpoPago);
		builder.append(", pcbsRutPropuesta=");
		builder.append(pcbsRutPropuesta);
		builder.append(", pcbsFchCotizacion=");
		builder.append(pcbsFchCotizacion);
		builder.append(", pcbsCorreo=");
		builder.append(pcbsCorreo);
		builder.append(", pcbsTpoVenta=");
		builder.append(pcbsTpoVenta);
		builder.append(", pcbsCodMail=");
		builder.append(pcbsCodMail);
		builder.append(", pcbsComuna=");
		builder.append(pcbsComuna);
		builder.append(", pcbsRegion=");
		builder.append(pcbsRegion);
		builder.append(", empIdDespacho=");
		builder.append(empIdDespacho);
		builder.append(", empIdImprenta=");
		builder.append(empIdImprenta);
		builder.append(", txId=");
		builder.append(txId);
		builder.append(", planId=");
		builder.append(planId);
		builder.append(", pptaId=");
		builder.append(pptaId);
		builder.append(", error=");
		builder.append(error);
		builder.append("]");
		return builder.toString();
	}
	
}
