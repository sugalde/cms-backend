package com.cencosud.sim.model;

import java.io.Serializable;

public class EmpresaModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int empId;
	private Long empRut;
	private String empRazonSocial;
	private String empTipo;
	private String empGiro;
	private String empFolio;
	private int comId;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public Long getEmpRut() {
		return empRut;
	}
	public void setEmpRut(Long empRut) {
		this.empRut = empRut;
	}
	public String getEmpRazonSocial() {
		return empRazonSocial;
	}
	public void setEmpRazonSocial(String empRazonSocial) {
		this.empRazonSocial = empRazonSocial;
	}
	public String getEmpTipo() {
		return empTipo;
	}
	public void setEmpTipo(String empTipo) {
		this.empTipo = empTipo;
	}
	public String getEmpGiro() {
		return empGiro;
	}
	public void setEmpGiro(String empGiro) {
		this.empGiro = empGiro;
	}
	public String getEmpFolio() {
		return empFolio;
	}
	public void setEmpFolio(String empFolio) {
		this.empFolio = empFolio;
	}
	public int getComId() {
		return comId;
	}
	public void setComId(int comId) {
		this.comId = comId;
	}
	@Override
	public String toString() {
		return "EmpresaModel [empId=" + empId + ", empRut=" + empRut + ", empRazonSocial=" + empRazonSocial
				+ ", empTipo=" + empTipo + ", empGiro=" + empGiro + ", empFolio=" + empFolio + ", comId=" + comId + "]";
	}
}
