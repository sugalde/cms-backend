package com.cencosud.sim.model;

import java.io.Serializable;

public class ErrorsModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int codigo;
	private String descripcion;
	
	private int txId;
	private int pllaId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	public int getPllaId() {
		return pllaId;
	}
	public void setPllaId(int pllaId) {
		this.pllaId = pllaId;
	}
	@Override
	public String toString() {
		return "ErrorsModel [id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + ", txId=" + txId
				+ ", pllaId=" + pllaId + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + id;
		result = prime * result + pllaId;
		result = prime * result + txId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorsModel other = (ErrorsModel) obj;
		if (codigo != other.codigo)
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (id != other.id)
			return false;
		if (pllaId != other.pllaId)
			return false;
		if (txId != other.txId)
			return false;
		return true;
	}
	
}
