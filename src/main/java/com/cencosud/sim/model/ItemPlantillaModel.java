package com.cencosud.sim.model;

import java.io.Serializable;

public class ItemPlantillaModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int itplId;
	private int pllaId;
	private int itemId;
	public int getItplId() {
		return itplId;
	}
	public void setItplId(int itplId) {
		this.itplId = itplId;
	}
	public int getPllaId() {
		return pllaId;
	}
	public void setPllaId(int pllaId) {
		this.pllaId = pllaId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	@Override
	public String toString() {
		return "ItemPlantillaModel [itplId=" + itplId + ", pllaId=" + pllaId + ", itemId=" + itemId + "]";
	}
	public ItemPlantillaModel(int itplId, int pllaId, int itemId) {
		super();
		this.itplId = itplId;
		this.pllaId = pllaId;
		this.itemId = itemId;
	}
	public ItemPlantillaModel() {
		super();
	}
	
}

