package com.cencosud.sim.model;

import java.io.Serializable;

public class ItemPropuestaModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String itptValor;
	private int itemId;
	private int pptaId;
	private String itemConsultaSql;
	
	public String getItptValor() {
		return itptValor;
	}
	public void setItptValor(String itptValor) {
		this.itptValor = itptValor;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getPptaId() {
		return pptaId;
	}
	public void setPptaId(int pptaId) {
		this.pptaId = pptaId;
	}
	public String getItemConsultaSql() {
		return itemConsultaSql;
	}
	public void setItemConsultaSql(String itemConsultaSql) {
		this.itemConsultaSql = itemConsultaSql;
	}
	@Override
	public String toString() {
		return "ItemPropuestaModel [itptValor=" + itptValor + ", itemId=" + itemId + ", pptaId=" + pptaId
				+ ", itemConsultaSql=" + itemConsultaSql + "]";
	}
	
}
