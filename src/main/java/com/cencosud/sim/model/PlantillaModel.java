package com.cencosud.sim.model;

import java.io.Serializable;

public class PlantillaModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pllaId;
	private String pllaRuta;
	
	public int getPllaId() {
		return pllaId;
	}
	public void setPllaId(int pllaId) {
		this.pllaId = pllaId;
	}
	public String getPllaRuta() {
		return pllaRuta;
	}
	public void setPllaRuta(String pllaRuta) {
		this.pllaRuta = pllaRuta;
	}
	@Override
	public String toString() {
		return "PlantillaModel [pllaId=" + pllaId + ", pllaRuta=" + pllaRuta + "]";
	}
}
