package com.cencosud.sim.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

import com.cencosud.sim.util.ComparatorStaticImpl;

public class PlantillaPropuestaModel implements Serializable, Comparable<PlantillaPropuestaModel>{

	private static final long serialVersionUID = 1L;
	
	private String rut;
	private int pptaId;
	private int docId;
	private int pllaId;
	private String pllaRuta;
	private int planId;
	
	private Map<String, Object> modeloDatos;
	
	public int getPllaId() {
		return pllaId;
	}
	public void setPllaId(int pllaId) {
		this.pllaId = pllaId;
	}
	public String getPllaRuta() {
		return pllaRuta;
	}
	public void setPllaRuta(String pllaRuta) {
		this.pllaRuta = pllaRuta;
	}
	public int getPptaId() {
		return pptaId;
	}
	public void setPptaId(int pptaId) {
		this.pptaId = pptaId;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	
	public int getDocId() {
		return docId;
	}
	
	public void setDocId(int docId) {
		this.docId = docId;
	}
	
	public Map<String, Object> getModeloDatos() {
		return modeloDatos;
	}
	public void setModeloDatos(Map<String, Object> modeloDatos) {
		this.modeloDatos = modeloDatos;
	}
	
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}

	@Override
	public String toString() {
		return "PlantillaPropuestaModel [rut=" + rut + ", pptaId=" + pptaId + ", docId=" + docId + ", pllaId=" + pllaId
				+ ", pllaRuta=" + pllaRuta + ", planId=" + planId + ", modeloDatos=" + modeloDatos + "]";
	}

	//Implementacion de la comparacion natural de los objetos propuesta-doc-planilla
	private static Comparator<PlantillaPropuestaModel> metadataComparator = Comparator
	        .comparing(PlantillaPropuestaModel::getPptaId, ComparatorStaticImpl.nullSafeIntComparator)
	        .thenComparing(PlantillaPropuestaModel::getDocId, ComparatorStaticImpl.docIdComparator)
	        .thenComparing(PlantillaPropuestaModel::getPptaId, ComparatorStaticImpl.nullSafeIntComparator);
	
	@Override
	public int compareTo(PlantillaPropuestaModel model) {
		return metadataComparator.compare(this, model);
	}
}
