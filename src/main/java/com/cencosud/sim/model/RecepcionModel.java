package com.cencosud.sim.model;

import java.io.Serializable;

public class RecepcionModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int recId;
	private String recFchArchivo;
	private String recNombreArchivo;
	private String recRutaArchivo;
	private int recRegistros;
	private int empresa;
	public int getRecId() {
		return recId;
	}
	public void setRecId(int recId) {
		this.recId = recId;
	}
	public String getRecFchArchivo() {
		return recFchArchivo;
	}
	public void setRecFchArchivo(String recFchArchivo) {
		this.recFchArchivo = recFchArchivo;
	}
	public String getRecNombreArchivo() {
		return recNombreArchivo;
	}
	public void setRecNombreArchivo(String recNombreArchivo) {
		this.recNombreArchivo = recNombreArchivo;
	}
	public String getRecRutaArchivo() {
		return recRutaArchivo;
	}
	public void setRecRutaArchivo(String recRutaArchivo) {
		this.recRutaArchivo = recRutaArchivo;
	}
	public int getRecRegistros() {
		return recRegistros;
	}
	public void setRecRegistros(int recRegistros) {
		this.recRegistros = recRegistros;
	}
	public int getEmpresa() {
		return empresa;
	}
	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RecepcionModel [recId=");
		builder.append(recId);
		builder.append(", recFchArchivo=");
		builder.append(recFchArchivo);
		builder.append(", recNombreArchivo=");
		builder.append(recNombreArchivo);
		builder.append(", recRutaArchivo=");
		builder.append(recRutaArchivo);
		builder.append(", recRegistros=");
		builder.append(recRegistros);
		builder.append(", empresa=");
		builder.append(empresa);
		builder.append("]");
		return builder.toString();
	}
	
}
