package com.cencosud.sim.model;

import java.io.Serializable;
import java.util.Date;

public class RespuestaModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int respId;
	private Long respEnvio;
	private String respReferencia;
	private String respEstado;
	private String respIncidencia;
	private Date respFchAdmision;
	private Date respFchEntrega;
	private String respNombre;
	private String respDireccion;
	private String respComuna;
	private int respOt;
	private int respIdCliente;
	private String respProyecto;
	private int respCor;
	private String respServicio;
	private Date respFchEnvio;
	private String respEnviado;
	private String respCorreo;
	private int respCodigoPdf;
	private String respSmtp;
	private String respTipoRebote;
	private String respReglaRebote;
	private Date respFchRebote;
	private Date respFchRecepcion;
	private int respPptaId;
	private int respTxId;
	private int recId;
	private int txIdOriginal;
	private int tptxId;
	public int getRespId() {
		return respId;
	}
	public void setRespId(int respId) {
		this.respId = respId;
	}
	public Long getRespEnvio() {
		return respEnvio;
	}
	public void setRespEnvio(Long respEnvio) {
		this.respEnvio = respEnvio;
	}
	public String getRespReferencia() {
		return respReferencia;
	}
	public void setRespReferencia(String respReferencia) {
		this.respReferencia = respReferencia;
	}
	public String getRespEstado() {
		return respEstado;
	}
	public void setRespEstado(String respEstado) {
		this.respEstado = respEstado;
	}
	public String getRespIncidencia() {
		return respIncidencia;
	}
	public void setRespIncidencia(String respIncidencia) {
		this.respIncidencia = respIncidencia;
	}
	public Date getRespFchAdmision() {
		return respFchAdmision;
	}
	public void setRespFchAdmision(Date respFchAdmision) {
		this.respFchAdmision = respFchAdmision;
	}
	public Date getRespFchEntrega() {
		return respFchEntrega;
	}
	public void setRespFchEntrega(Date respFchEntrega) {
		this.respFchEntrega = respFchEntrega;
	}
	public String getRespNombre() {
		return respNombre;
	}
	public void setRespNombre(String respNombre) {
		this.respNombre = respNombre;
	}
	public String getRespDireccion() {
		return respDireccion;
	}
	public void setRespDireccion(String respDireccion) {
		this.respDireccion = respDireccion;
	}
	public String getRespComuna() {
		return respComuna;
	}
	public void setRespComuna(String respComuna) {
		this.respComuna = respComuna;
	}
	public int getRespOt() {
		return respOt;
	}
	public void setRespOt(int respOt) {
		this.respOt = respOt;
	}
	public int getRespIdCliente() {
		return respIdCliente;
	}
	public void setRespIdCliente(int respIdCliente) {
		this.respIdCliente = respIdCliente;
	}
	public String getRespProyecto() {
		return respProyecto;
	}
	public void setRespProyecto(String respProyecto) {
		this.respProyecto = respProyecto;
	}
	public int getRespCor() {
		return respCor;
	}
	public void setRespCor(int respCor) {
		this.respCor = respCor;
	}
	public String getRespServicio() {
		return respServicio;
	}
	public void setRespServicio(String respServicio) {
		this.respServicio = respServicio;
	}
	public Date getRespFchEnvio() {
		return respFchEnvio;
	}
	public void setRespFchEnvio(Date respFchEnvio) {
		this.respFchEnvio = respFchEnvio;
	}
	public String getRespEnviado() {
		return respEnviado;
	}
	public void setRespEnviado(String respEnviado) {
		this.respEnviado = respEnviado;
	}
	public String getRespCorreo() {
		return respCorreo;
	}
	public void setRespCorreo(String respCorreo) {
		this.respCorreo = respCorreo;
	}
	public int getRespCodigoPdf() {
		return respCodigoPdf;
	}
	public void setRespCodigoPdf(int respCodigoPdf) {
		this.respCodigoPdf = respCodigoPdf;
	}
	public String getRespSmtp() {
		return respSmtp;
	}
	public void setRespSmtp(String respSmtp) {
		this.respSmtp = respSmtp;
	}
	public String getRespTipoRebote() {
		return respTipoRebote;
	}
	public void setRespTipoRebote(String respTipoRebote) {
		this.respTipoRebote = respTipoRebote;
	}
	public String getRespReglaRebote() {
		return respReglaRebote;
	}
	public void setRespReglaRebote(String respReglaRebote) {
		this.respReglaRebote = respReglaRebote;
	}
	public Date getRespFchRebote() {
		return respFchRebote;
	}
	public void setRespFchRebote(Date respFchRebote) {
		this.respFchRebote = respFchRebote;
	}
	public Date getRespFchRecepcion() {
		return respFchRecepcion;
	}
	public void setRespFchRecepcion(Date respFchRecepcion) {
		this.respFchRecepcion = respFchRecepcion;
	}
	public int getRespPptaId() {
		return respPptaId;
	}
	public void setRespPptaId(int respPptaId) {
		this.respPptaId = respPptaId;
	}
	public int getRespTxId() {
		return respTxId;
	}
	public void setRespTxId(int respTxId) {
		this.respTxId = respTxId;
	}
	public int getRecId() {
		return recId;
	}
	public void setRecId(int recId) {
		this.recId = recId;
	}
	public int getTxIdOriginal() {
		return txIdOriginal;
	}
	public void setTxIdOriginal(int txIdOriginal) {
		this.txIdOriginal = txIdOriginal;
	}
	public int getTptxId() {
		return tptxId;
	}
	public void setTptxId(int tptxId) {
		this.tptxId = tptxId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RespuestaModel [respId=");
		builder.append(respId);
		builder.append(", respEnvio=");
		builder.append(respEnvio);
		builder.append(", respReferencia=");
		builder.append(respReferencia);
		builder.append(", respEstado=");
		builder.append(respEstado);
		builder.append(", respIncidencia=");
		builder.append(respIncidencia);
		builder.append(", respFchAdmision=");
		builder.append(respFchAdmision);
		builder.append(", respFchEntrega=");
		builder.append(respFchEntrega);
		builder.append(", respNombre=");
		builder.append(respNombre);
		builder.append(", respDireccion=");
		builder.append(respDireccion);
		builder.append(", respComuna=");
		builder.append(respComuna);
		builder.append(", respOt=");
		builder.append(respOt);
		builder.append(", respIdCliente=");
		builder.append(respIdCliente);
		builder.append(", respProyecto=");
		builder.append(respProyecto);
		builder.append(", respCor=");
		builder.append(respCor);
		builder.append(", respServicio=");
		builder.append(respServicio);
		builder.append(", respFchEnvio=");
		builder.append(respFchEnvio);
		builder.append(", respEnviado=");
		builder.append(respEnviado);
		builder.append(", respCorreo=");
		builder.append(respCorreo);
		builder.append(", respCodigoPdf=");
		builder.append(respCodigoPdf);
		builder.append(", respSmtp=");
		builder.append(respSmtp);
		builder.append(", respTipoRebote=");
		builder.append(respTipoRebote);
		builder.append(", respReglaRebote=");
		builder.append(respReglaRebote);
		builder.append(", respFchRebote=");
		builder.append(respFchRebote);
		builder.append(", respFchRecepcion=");
		builder.append(respFchRecepcion);
		builder.append(", respPptaId=");
		builder.append(respPptaId);
		builder.append(", respTxId=");
		builder.append(respTxId);
		builder.append(", recId=");
		builder.append(recId);
		builder.append(", txIdOriginal=");
		builder.append(txIdOriginal);
		builder.append(", tptxId=");
		builder.append(tptxId);
		builder.append("]");
		return builder.toString();
	}
	
}
