package com.cencosud.sim.model;

import java.io.Serializable;
import java.util.Date;

public class TransaccionModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int txId;
	private Date txFchInicio;
	private Date txFchTermino;
	private String txOk;
	private String txRespuesta;
	private int empId;
	private String txUsuario;
	private int estId;
	private int tptxId;
	private int txIdPadre;
	private String txFchInicioCarga;
	private String txFchTerminoCarga;
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	public Date getTxFchInicio() {
		return txFchInicio;
	}
	public void setTxFchInicio(Date txFchInicio) {
		this.txFchInicio = txFchInicio;
	}
	public Date getTxFchTermino() {
		return txFchTermino;
	}
	public void setTxFchTermino(Date txFchTermino) {
		this.txFchTermino = txFchTermino;
	}
	public String getTxOk() {
		return txOk;
	}
	public void setTxOk(String txOk) {
		this.txOk = txOk;
	}
	public String getTxRespuesta() {
		return txRespuesta;
	}
	public void setTxRespuesta(String txRespuesta) {
		this.txRespuesta = txRespuesta;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getTxUsuario() {
		return txUsuario;
	}
	public void setTxUsuario(String txUsuario) {
		this.txUsuario = txUsuario;
	}
	public int getEstId() {
		return estId;
	}
	public void setEstId(int estId) {
		this.estId = estId;
	}
	public int getTptxId() {
		return tptxId;
	}
	public void setTptxId(int tptxId) {
		this.tptxId = tptxId;
	}
	public int getTxIdPadre() {
		return txIdPadre;
	}
	public void setTxIdPadre(int txIdPadre) {
		this.txIdPadre = txIdPadre;
	}
	public String getTxFchInicioCarga() {
		return txFchInicioCarga;
	}
	public void setTxFchInicioCarga(String txFchInicioCarga) {
		this.txFchInicioCarga = txFchInicioCarga;
	}
	public String getTxFchTerminoCarga() {
		return txFchTerminoCarga;
	}
	public void setTxFchTerminoCarga(String txFchTerminoCarga) {
		this.txFchTerminoCarga = txFchTerminoCarga;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransaccionModel [txId=");
		builder.append(txId);
		builder.append(", txFchInicio=");
		builder.append(txFchInicio);
		builder.append(", txFchTermino=");
		builder.append(txFchTermino);
		builder.append(", txOk=");
		builder.append(txOk);
		builder.append(", txRespuesta=");
		builder.append(txRespuesta);
		builder.append(", empId=");
		builder.append(empId);
		builder.append(", txUsuario=");
		builder.append(txUsuario);
		builder.append(", estId=");
		builder.append(estId);
		builder.append(", tptxId=");
		builder.append(tptxId);
		builder.append(", txIdPadre=");
		builder.append(txIdPadre);
		builder.append(", txFchInicioCarga=");
		builder.append(txFchInicioCarga);
		builder.append(", txFchTerminoCarga=");
		builder.append(txFchTerminoCarga);
		builder.append("]");
		return builder.toString();
	}
}
