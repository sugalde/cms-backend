package com.cencosud.sim.model.request;

import java.io.Serializable;

public class CicloRespuestaRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String usuario;
	private boolean crear;
	private String archivo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public boolean isCrear() {
		return crear;
	}
	public void setCrear(boolean crear) {
		this.crear = crear;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CicloRespuestaRequest [id=");
		builder.append(id);
		builder.append(", usuario=");
		builder.append(usuario);
		builder.append(", crear=");
		builder.append(crear);
		builder.append(", archivo=");
		builder.append(archivo);
		builder.append("]");
		return builder.toString();
	}
}
