package com.cencosud.sim.model.request;

import java.io.Serializable;

public class ExcelRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idSim;
	private String opcion;
	private int recId;
	public int getIdSim() {
		return idSim;
	}
	public void setIdSim(int idSim) {
		this.idSim = idSim;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public int getRecId() {
		return recId;
	}
	public void setRecId(int recId) {
		this.recId = recId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExcelRequest [idSim=");
		builder.append(idSim);
		builder.append(", opcion=");
		builder.append(opcion);
		builder.append(", recId=");
		builder.append(recId);
		builder.append("]");
		return builder.toString();
	}
}
