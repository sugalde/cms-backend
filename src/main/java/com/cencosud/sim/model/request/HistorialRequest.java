package com.cencosud.sim.model.request;

import java.io.Serializable;

public class HistorialRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String desde;
	private String hasta;
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HistorialRequest [desde=");
		builder.append(desde);
		builder.append(", hasta=");
		builder.append(hasta);
		builder.append("]");
		return builder.toString();
	}
	
}
