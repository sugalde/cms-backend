package com.cencosud.sim.model.request;

import java.io.Serializable;

public class PropuestaValorRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pptaId;
	private int planId;
	private String itptValor;
	private String itemDescripcion;
	private int pllaId;
	private String pllaRuta;
	private int itemId;
	public Long getPptaId() {
		return pptaId;
	}
	public void setPptaId(Long pptaId) {
		this.pptaId = pptaId;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getItptValor() {
		return itptValor;
	}
	public void setItptValor(String itptValor) {
		this.itptValor = itptValor;
	}
	public String getItemDescripcion() {
		return itemDescripcion;
	}
	public void setItemDescripcion(String itemDescripcion) {
		this.itemDescripcion = itemDescripcion;
	}
	public int getPllaId() {
		return pllaId;
	}
	public void setPllaId(int pllaId) {
		this.pllaId = pllaId;
	}
	public String getPllaRuta() {
		return pllaRuta;
	}
	public void setPllaRuta(String pllaRuta) {
		this.pllaRuta = pllaRuta;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	@Override
	public String toString() {
		return "PropuestaValorRequest [pptaId=" + pptaId + ", planId=" + planId + ", itptValor=" + itptValor
				+ ", itemDescripcion=" + itemDescripcion + ", pllaId=" + pllaId + ", pllaRuta=" + pllaRuta + ", itemId="
				+ itemId + "]";
	}
}
