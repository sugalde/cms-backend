package com.cencosud.sim.model.response;

import java.io.Serializable;

public class MessageResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String descripcion;
	private int registros;
	private int procesados;
	private int errores;
	private int estadoId;
	private String estado;
	private boolean error;
	private String detalle;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getRegistros() {
		return registros;
	}
	public void setRegistros(int registros) {
		this.registros = registros;
	}
	public int getProcesados() {
		return procesados;
	}
	public void setProcesados(int procesados) {
		this.procesados = procesados;
	}
	public int getErrores() {
		return errores;
	}
	public void setErrores(int errores) {
		this.errores = errores;
	}
	public int getEstadoId() {
		return estadoId;
	}
	public void setEstadoId(int estadoId) {
		this.estadoId = estadoId;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageResponse [id=");
		builder.append(id);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", registros=");
		builder.append(registros);
		builder.append(", procesados=");
		builder.append(procesados);
		builder.append(", errores=");
		builder.append(errores);
		builder.append(", estadoId=");
		builder.append(estadoId);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", error=");
		builder.append(error);
		builder.append(", detalle=");
		builder.append(detalle);
		builder.append("]");
		return builder.toString();
	}
}
