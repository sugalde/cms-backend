package com.cencosud.sim.model.response;

import java.io.Serializable;

public class ResEmpresaResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int leido;
	private int entregado;
	private int noEntregado;
	private boolean error;
	private String descripcion;
	public int getLeido() {
		return leido;
	}
	public void setLeido(int leido) {
		this.leido = leido;
	}
	public int getEntregado() {
		return entregado;
	}
	public void setEntregado(int entregado) {
		this.entregado = entregado;
	}
	public int getNoEntregado() {
		return noEntregado;
	}
	public void setNoEntregado(int noEntregado) {
		this.noEntregado = noEntregado;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResEmpresaResponse [leido=");
		builder.append(leido);
		builder.append(", entregado=");
		builder.append(entregado);
		builder.append(", noEntregado=");
		builder.append(noEntregado);
		builder.append(", error=");
		builder.append(error);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append("]");
		return builder.toString();
	}
	
}
