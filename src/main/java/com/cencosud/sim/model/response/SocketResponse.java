package com.cencosud.sim.model.response;

import java.io.Serializable;

public class SocketResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String saludo;
	private String response;
	private int id;
	private int porcentaje;
	private String mensaje;
	private String detalle;
	public String getSaludo() {
		return saludo;
	}
	public void setSaludo(String saludo) {
		this.saludo = saludo;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public SocketResponse(String saludo) {
		super();
		this.saludo = saludo;
	}
	public SocketResponse() {
		super();
	}
	public SocketResponse(int id, int porcentaje, String mensaje, String detalle, String response) {
		super();
		this.response = response;
		this.id = id;
		this.porcentaje = porcentaje;
		this.mensaje = mensaje;
		this.detalle = detalle;
	}
	
}
