package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.EmpresaModel;
import com.cencosud.sim.model.request.EmpresaRequest;

public interface EmpresaService {

	List<EmpresaModel> obtenerEmpresaRecepcion(EmpresaRequest empresaRequest);

	EmpresaModel obtenerEmpresaXID(int empId);
	
	
}
