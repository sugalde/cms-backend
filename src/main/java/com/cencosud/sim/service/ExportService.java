package com.cencosud.sim.service;

import org.springframework.ui.Model;

import com.cencosud.sim.model.request.ExcelRequest;

public interface ExportService {

	void generarPropuestas(Model model, ExcelRequest excelRequest);

	void obtenerArchivoRespuesta(Model model, ExcelRequest excelRequest);

}
