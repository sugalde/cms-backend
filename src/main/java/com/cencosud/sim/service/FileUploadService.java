package com.cencosud.sim.service;


import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cencosud.sim.model.response.ResEmpresaResponse;

public interface FileUploadService {

	ResEmpresaResponse uploadFile(MultipartHttpServletRequest request);

}
