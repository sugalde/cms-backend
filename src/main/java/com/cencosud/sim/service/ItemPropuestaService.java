package com.cencosud.sim.service;

import java.util.List;
import java.util.Map;

import com.cencosud.sim.model.ItemPropuestaModel;
import com.cencosud.sim.model.request.PropuestaValorRequest;

public interface ItemPropuestaService {

	List<ItemPropuestaModel> obtenerItemPropuestas(int pptaId);

	int insertarItemsPropuesta(ItemPropuestaModel itemPropuestaModel2);

	Long obtenerValorItem(ItemPropuestaModel itemPropuestaModel);

	Map<String, Object> obtenerItemPropuesta(int pllaId, int pptaId);

	void ejecutarQuery(String query);

	List<Long> obtenerItemPropuestasMas(int itemId, int txId);

	int insertarItemsPropuestaMas(List<ItemPropuestaModel> itemPropuestas);

	List<Long> obtenerItems(int txId);

	List<PropuestaValorRequest> cuerpoPropuestaValor(int txId);

	String obtenerQuery(int itemId);


}
