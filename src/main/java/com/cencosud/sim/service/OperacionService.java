package com.cencosud.sim.service;

import java.util.Map;

import com.cencosud.sim.model.request.CargaPcbsRequest;

public interface OperacionService {

	int registrarErrores(Map<String, Integer> error);

	void quitarErrores(int txId);

	int validarComuna(CargaPcbsRequest cargaPcbsRequest);

	Long obtenerErrores(int txId);

}
