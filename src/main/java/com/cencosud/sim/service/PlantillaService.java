package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.PlantillaModel;
import com.cencosud.sim.model.PlantillaPropuestaModel;

public interface PlantillaService {

	List<PlantillaModel> obtenerPlantillas(int txId);

	List<PlantillaModel> buscarPlantillas(int txId);

	List<PlantillaPropuestaModel> buscarPlantilla(int txId);

	void guardarDatosPlantillas(List<PlantillaModel> plantillaList);

}
