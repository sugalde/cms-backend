package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.RespuestaModel;

public interface RespuestaService {

	void registrarRespuesta(List<RespuestaModel> respuestas, int recId, int empresa);

	List<RespuestaModel> obtenerRespuestasEmail(int recId);

}
