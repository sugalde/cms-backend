package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.TipoTransaccionModel;

public interface TipoTransaccionService {

	List<TipoTransaccionModel> obtenerTipoTransaccion();

}
