package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.TrackingModel;
import com.cencosud.sim.model.request.TrackingRequest;

public interface TrackingService {

	List<TrackingModel> getTracking(TrackingRequest trackingRequest);

}
