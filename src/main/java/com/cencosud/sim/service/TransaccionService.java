package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.model.response.MessageResponse;

public interface TransaccionService {

	TransaccionModel registrarTransaccion(CargaPcbsRequest cargaPcbsRequest);

	int guardarTransaccion(TransaccionModel transaccionModel);

	List<CargasPcbsResponse> obtenerTransacciones();

	CargasPcbsResponse obtenerTransaccion(int txId);

	TransaccionModel obtenerTransaccionPadre(int txId);

	TransaccionModel obtenerUltimaTransaccion(int txId);

	MessageResponse obtenerMensaje(CargaPcbsRequest cargaPcbsRequest);

	void guardarRutaArchivo(int propuesta, String nombreArchivo, String ruta, int txId);

}
