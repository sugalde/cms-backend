package com.cencosud.sim.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.model.CargaPcbsModel;
import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.model.request.ExcelRequest;
import com.cencosud.sim.service.CargaPcbsService;
import com.cencosud.sim.service.ExportService;
import com.cencosud.sim.service.RecepcionService;
import com.cencosud.sim.service.TransaccionService;
import com.cencosud.sim.util.ArchivoServidor;

@Service("ExportService")
public class ExportServiceImpl implements ExportService{
	static final Logger log = Logger.getLogger(ExportServiceImpl.class);
	
	@Autowired
	@Qualifier("CargaPcbsService")
	private CargaPcbsService cargaPcbsService; 
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	@Autowired
	@Qualifier("RecepcionService")
	private RecepcionService recepcionService;
	
	@Override
	public void generarPropuestas(Model model, ExcelRequest excelRequest) {
		try {
			Map<Integer, String> encabezado = encabezadoCargaPcbs();
			List<Map<Integer, Object>> cuerpo = cuerpoCargaPcbs(excelRequest);
			model.addAttribute("cuerpo", cuerpo);
			model.addAttribute("encabezado",encabezado);
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	/**
	 * Obtiene el cuerpo del archivo excel para la carga_pcbs
	 * @param excelRequest
	 * @return
	 */
	private List<Map<Integer, Object>> cuerpoCargaPcbs(ExcelRequest excelRequest) {
		List<Map<Integer, Object>> cuerpo = new ArrayList<>();
		List<CargaPcbsModel> cargaPcbsList = cargaPcbsService.obtenerCargaPcbs(excelRequest);
		for (CargaPcbsModel cargaPcbs : cargaPcbsList) {
			Map<Integer, Object> carga = new HashMap<>();
			carga.put(0,cargaPcbs.getPcbsId());
			carga.put(1,cargaPcbs.getPcbsCodCompania());
			carga.put(2,cargaPcbs.getPcbsCodNegocio());
			carga.put(3,cargaPcbs.getPcbsCodRamo());
			carga.put(4,cargaPcbs.getPcbsTpoPago());
			carga.put(5,cargaPcbs.getPcbsRutPropuesta());
			carga.put(6,cargaPcbs.getPcbsFchCotizacion());
			carga.put(7,cargaPcbs.getPcbsCorreo());
			carga.put(8,cargaPcbs.getPcbsTpoVenta());
			carga.put(9,cargaPcbs.getPcbsCodMail());
			carga.put(10,cargaPcbs.getPcbsComuna());
			carga.put(11,cargaPcbs.getPcbsRegion());
			carga.put(12,cargaPcbs.getEmpIdDespacho());
			carga.put(13,cargaPcbs.getEmpIdImprenta());
			carga.put(14,cargaPcbs.getTxId());
			carga.put(15,cargaPcbs.getPlanId());
			carga.put(16,cargaPcbs.getPptaId());
			carga.put(17, cargaPcbs.getError());
			cuerpo.add(carga);
		}
		return cuerpo;
	}

	/**
	 * Obtiene los encabezados para configurar el archivo excel al tipo carga_pcbs
	 * @return
	 */
	private Map<Integer, String> encabezadoCargaPcbs() {
		Map<Integer, String> encabezado = new HashMap<>();
		encabezado.put(0,"Id");
		encabezado.put(1,"Código Compañía");
		encabezado.put(2,"Código Negocio");
		encabezado.put(3,"Código Ramo");
		encabezado.put(4,"Tipo Pago");
		encabezado.put(5,"Rut Propuesta");
		encabezado.put(6,"Fecha Cotización");
		encabezado.put(7,"Correo");
		encabezado.put(8,"Tipo Venta");
		encabezado.put(9,"Código Mail");
		encabezado.put(10,"Comuna");
		encabezado.put(11,"Región");
		encabezado.put(12,"Id empresa despacho");
		encabezado.put(13,"Id empresa imprenta");
		encabezado.put(14,"Id Transacción");
		encabezado.put(15,"Id Plan");
		encabezado.put(16,"Propuesta");
		encabezado.put(17, "Error");
		return encabezado;
	}


	@Override
	public void obtenerArchivoRespuesta(Model model, ExcelRequest excelRequest) {
		try {
			FileUploadServiceImpl fileUploadServiceImpl = new FileUploadServiceImpl(); 
			Map<String,Object> contenido = obtenerContenidoArchivo(excelRequest);
			@SuppressWarnings("unchecked")
			Map<Integer,String> encabezado = (Map<Integer,String>) contenido.get("clave");
			@SuppressWarnings("unchecked")
			List<Map<String,Object>> lectura = (List<Map<String, Object>>) contenido.get("lectura");
			int empresa = (int) contenido.get("empresa");
			
			List<RespuestaModel> respuestas = fileUploadServiceImpl.agregarData(lectura,empresa,excelRequest.getRecId());
			
			List<Map<Integer, Object>> cuerpo = cuerpoArchivoRespuesta(respuestas,empresa);
			
			model.addAttribute("cuerpo", cuerpo);
			model.addAttribute("encabezado",encabezado);
		} catch (Exception e) {
			log.error(e);
		}
		
	}

	private List<Map<Integer, Object>> cuerpoArchivoRespuesta(List<RespuestaModel> respuestas, int empresa) {
		List<Map<Integer, Object>> cuerpo = null;
		if(Constants.EMPRESA_CORREO_ELECTRONICO == empresa) {
			cuerpo = llenarCuerpoEmail(respuestas);
		}else {
			cuerpo = llenarCuerpoTradicional(respuestas);
		}
		return cuerpo;
	}

	private List<Map<Integer, Object>> llenarCuerpoTradicional(List<RespuestaModel> respuestas) {
		List<Map<Integer, Object>> cuerpo = new ArrayList<>();
		SimpleDateFormat diaMesAnio = new SimpleDateFormat(Constants.FECHA_FRONT);
		for (RespuestaModel respuesta : respuestas) {
			Map<Integer, Object> carga = new HashMap<>();
			carga.put(0,respuesta.getRespEnvio());
			carga.put(1,respuesta.getRespReferencia());
			carga.put(2,respuesta.getRespEstado());
			carga.put(3,respuesta.getRespIncidencia());
			carga.put(4,respuesta.getRespFchAdmision()==null?"":diaMesAnio.format(respuesta.getRespFchAdmision()));
			carga.put(5,respuesta.getRespFchEntrega()==null?"":diaMesAnio.format(respuesta.getRespFchEntrega()));
			carga.put(6,respuesta.getRespNombre());
			carga.put(7,respuesta.getRespDireccion());
			carga.put(8,respuesta.getRespComuna());
			carga.put(9,respuesta.getRespOt());
			carga.put(10,respuesta.getRespIdCliente());
			carga.put(11,respuesta.getRespProyecto());
			carga.put(12,respuesta.getRespCor());
			carga.put(13,respuesta.getRespServicio());
			cuerpo.add(carga);
		}
		return cuerpo;
	}

	private List<Map<Integer, Object>> llenarCuerpoEmail(List<RespuestaModel> respuestas) {
		List<Map<Integer, Object>> cuerpo = new ArrayList<>();
		SimpleDateFormat diaMesAnio = new SimpleDateFormat(Constants.FECHA_FRONT);
		SimpleDateFormat hora = new SimpleDateFormat("HH:mm:ss");
		for (RespuestaModel respuesta : respuestas) {
			Map<Integer, Object> carga = new HashMap<>();
			carga.put(0,respuesta.getRespFchEnvio()==null?"":diaMesAnio.format(respuesta.getRespFchEnvio()));
			carga.put(1,respuesta.getRespFchEnvio()==null?"":hora.format(respuesta.getRespFchEnvio()));
			carga.put(2,respuesta.getRespEnviado());
			carga.put(3,respuesta.getRespCorreo());
			carga.put(4,respuesta.getRespCodigoPdf());
			carga.put(5,respuesta.getRespSmtp());
			carga.put(6,respuesta.getRespEstado());
			carga.put(7,respuesta.getRespReferencia());
			carga.put(8,respuesta.getRespTipoRebote());
			carga.put(9,respuesta.getRespReglaRebote());
			carga.put(10,respuesta.getRespFchRebote()==null?"":diaMesAnio.format(respuesta.getRespFchRebote()));
			carga.put(11,respuesta.getRespFchRecepcion()==null?"":diaMesAnio.format(respuesta.getRespFchRecepcion()));
			cuerpo.add(carga);
		}
		return cuerpo;
	}

	private Map<String,Object> obtenerContenidoArchivo(ExcelRequest excelRequest) {
		RecepcionModel recepcion = recepcionService.obtenerRecepcion(excelRequest.getRecId());
		ArchivoServidor archivo = new ArchivoServidor();
		File archivoExcel = archivo.obtenerArchivoSmb("smb:"+recepcion.getRecRutaArchivo());
		
		String[] archivoDesc = recepcion.getRecNombreArchivo().split("\\.");
		String extension = archivoDesc[archivoDesc.length -1];
		
		FileUploadServiceImpl fileUploadServiceImpl = new FileUploadServiceImpl(); 		
		Map<String,Object> respuesta = fileUploadServiceImpl.leerArchivo(extension, archivoExcel);
		respuesta.put("empresa", recepcion.getEmpresa());
		return respuesta;
	}

}
