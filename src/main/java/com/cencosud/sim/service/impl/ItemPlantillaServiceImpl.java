package com.cencosud.sim.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.mapper.ItemPlantillaMapper;
import com.cencosud.sim.model.ItemPlantillaModel;
import com.cencosud.sim.service.ItemPlantillaService;


@Service("ItemPlantillaService")
public class ItemPlantillaServiceImpl implements ItemPlantillaService{
	static final Logger log = Logger.getLogger(ItemPlantillaServiceImpl.class);
	
	@Autowired
	private ItemPlantillaMapper itemPlantillaMapper;
	
	
	@Override
	public List<ItemPlantillaModel> obtenerItemPlantillaFiltrado(List<ItemPlantillaModel> plantillasAprocesar) {
		//Obtiene los itemmPlantillas ya existentes en la base de datos para evitar duplicidad
		List<ItemPlantillaModel> itemPlantillaExistente = itemPlantillaMapper.obtenerItemPlantillaFiltrado(plantillasAprocesar);
		
		return filtrarItemPlantilla(plantillasAprocesar,itemPlantillaExistente);
	}
	
	/**
	 * Filtra lasa plantillas existentes vs los datos guardados en base de datos para evitar duplicidad
	 * @param plantillasAprocesar
	 * @param itemPlantillasExistentes
	 * @return
	 */
	private List<ItemPlantillaModel> filtrarItemPlantilla(List<ItemPlantillaModel> plantillasAprocesar, List<ItemPlantillaModel> itemPlantillasExistentes){
		try {
			for (ItemPlantillaModel itemPlantillaExistente : itemPlantillasExistentes) {
				for (int i = 0; i < plantillasAprocesar.size(); i++) {
					if(plantillasAprocesar.get(i).getPllaId()==itemPlantillaExistente.getPllaId() &&
						plantillasAprocesar.get(i).getItemId()==itemPlantillaExistente.getItemId()) {
						plantillasAprocesar.remove(i);
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
	return plantillasAprocesar;
}

	@Override
	public void guardarDatosPlantilla(List<ItemPlantillaModel> plantillasAGuardar) {
		itemPlantillaMapper.guardarDatosPlantilla(plantillasAGuardar);
	}

}
