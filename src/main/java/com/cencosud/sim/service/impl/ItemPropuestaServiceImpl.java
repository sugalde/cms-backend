package com.cencosud.sim.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.mapper.ItemPropuestaMapper;
import com.cencosud.sim.model.ItemPropuestaModel;
import com.cencosud.sim.model.request.PropuestaValorRequest;
import com.cencosud.sim.service.ItemPropuestaService;

@Service("ItemPropuestaService")
public class ItemPropuestaServiceImpl implements ItemPropuestaService{
	static final Logger log = Logger.getLogger(ItemPropuestaServiceImpl.class);
	@Autowired
	private ItemPropuestaMapper itemPropuestaMapper;

	@Override
	public List<ItemPropuestaModel> obtenerItemPropuestas(int pptaId) {
		return itemPropuestaMapper.obtenerItemPropuestas(pptaId);
	}

	@Override
	public int insertarItemsPropuesta(ItemPropuestaModel itemPropuestaModel) {
		return itemPropuestaMapper.insertarItemsPropuesta(itemPropuestaModel);
	}

	@Override
	public Long obtenerValorItem(ItemPropuestaModel itemPropuestaModel) {
		return itemPropuestaMapper.obtenerValorItem(itemPropuestaModel);
	}

	@Override
	public Map<String, Object> obtenerItemPropuesta(int pllaId, int pptaId) {
		Map<String, Object> model = new HashMap<>();
		try {
			List<ItemPropuestaModel> itemsPropuesta = itemPropuestaMapper.obtenerItemPropuesta(pllaId,pptaId);
			
			for (ItemPropuestaModel itemPropuestaModel : itemsPropuesta) {
				model.put(String.valueOf(itemPropuestaModel.getItemId()), itemPropuestaModel.getItptValor());
			}
		} catch (Exception e) {
			log.error(e);
		}
		
		return model;
	}

	@Override
	public void ejecutarQuery(String query) {
		try {
			 itemPropuestaMapper.ejecutarQuery(query);
		} catch (Exception e) {
			log.error(e);
		}
	}

	@Override
	public List<Long> obtenerItemPropuestasMas(int itemId, int txId) {
		return itemPropuestaMapper.obtenerItemPropuestasMas(itemId,txId);
	}

	@Override
	public int insertarItemsPropuestaMas(List<ItemPropuestaModel> itemPropuestas) {
		try {
			return itemPropuestaMapper.insertarItemsPropuestaMas(itemPropuestas);
		} catch (Exception e) {
			log.error(e);
			return 0;
		}
	}

	@Override
	public List<Long> obtenerItems(int txId) {
		List<Long> items = null;
		try {
			items = itemPropuestaMapper.obtenerItems(txId);
		} catch (Exception e) {
			log.error(e);
		}
		return items;
	}

	@Override
	public List<PropuestaValorRequest> cuerpoPropuestaValor(int txId) {
		return itemPropuestaMapper.cuerpoPropuestaValor(txId);
	}

	@Override
	public String obtenerQuery(int itemId) {
		return itemPropuestaMapper.obtenerQuery(itemId);
	}

}
