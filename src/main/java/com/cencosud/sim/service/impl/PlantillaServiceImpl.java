package com.cencosud.sim.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.cencosud.sim.mapper.PlantillaMapper;
import com.cencosud.sim.model.ItemPlantillaModel;
import com.cencosud.sim.model.PlantillaModel;
import com.cencosud.sim.model.PlantillaPropuestaModel;
import com.cencosud.sim.service.ItemPlantillaService;
import com.cencosud.sim.service.PlantillaService;
import com.cencosud.sim.util.ArchivoServidor;
import com.cencosud.sim.util.Util;

@Service("PlantillaService")
public class PlantillaServiceImpl implements PlantillaService{
	static final Logger log = Logger.getLogger(PlantillaServiceImpl.class);
	
	@Autowired
	private PlantillaMapper plantillaMapper;
	
	@Autowired
	@Qualifier("ItemPlantillaService")
	private ItemPlantillaService itemPlantillaService;

	@Override
	public List<PlantillaModel> obtenerPlantillas(int txId) {
		return plantillaMapper.buscarPlantillas(txId);
	}

	/**
	 * Guarda los items obtenidos de la plantilla en la tabla items_plantilla
	 * @param plantillas
	 */
	@Override
	public void guardarDatosPlantillas(List<PlantillaModel> plantillas) {
		//Obtiene listado de item_plantilla validos tomados desde archivos fisicos
		List<ItemPlantillaModel> plantillasAprocesar = procesarPlantillas(plantillas);
		if(!plantillasAprocesar.isEmpty()) {
			Util util = new Util();
			List<List<ItemPlantillaModel>> plantillasAGuardarList = util.cortarArray(plantillasAprocesar,1000);
			for (List<ItemPlantillaModel> plantilla : plantillasAGuardarList) {
				List<ItemPlantillaModel> plantillasAGuardar = itemPlantillaService.obtenerItemPlantillaFiltrado(plantilla);
				itemPlantillaService.guardarDatosPlantilla(plantillasAGuardar);
			}
			
		}else {
			log.info("TODAS LOS ITEM_PLANTILLA YA SE ENCUENTRAN REGISTRADOS");
		}
	}
	
			
	/**
	 * Procesa las plantilla armando un listado de objetos del tipo itemPlantilla validos para
	 * ser insertados en la tabla item_plantilla, si no existen plantillas no se agregaran al listado
	 * y se registrara en la tabla operacion para mostrar datos estadisticos en la pantalla de tracking
	 * @param plantillas
	 * @return
	 */
	private List<ItemPlantillaModel> procesarPlantillas(List<PlantillaModel> plantillas) {
		List<ItemPlantillaModel> itemPlantillaList = new ArrayList<>();
		ArchivoServidor archivo = new ArchivoServidor();
		//Itera listado de plantillas para lenar tabla item_plantilla
		for (PlantillaModel plantillaModel : plantillas) {
			
		    String filename = plantillaModel.getPllaRuta();
			
			//obtiene el contenido de la plantilla en String
			String[] contenido = {archivo.getStringHTML(filename)};
			
			//En caso de no existir la plantilla se agrega a la lista, 
			if(contenido[0].equals("")) {
				//TODO registrar error
				log.info("NO SE ENCONTRO LA PLANTILLA "+plantillaModel.getPllaId()+", "+plantillaModel.getPllaRuta());
				continue;
			}
			//Obtiene los items desde el contenido de la plantilla
			List<Integer> items = obtenerItems(contenido[0]);
			
			for (Integer item : items) {
					ItemPlantillaModel itemPlantilla = new ItemPlantillaModel();
					itemPlantilla.setItemId(item);
					itemPlantilla.setPllaId(plantillaModel.getPllaId());
					itemPlantillaList.add(itemPlantilla);
			}
		}
		return itemPlantillaList;
	}
	
		
	/**
	 * Obtiene el listado de items, entre corchetes [], que se encuentran en las plantillas 
	 * @param html
	 * @return
	 */
	private List<Integer> obtenerItems(String html) {
		List<Integer> items = new ArrayList<>();
        Matcher m = Pattern.compile("\\[(.*?)\\]").matcher(html);
        while(m.find()) {
        	try {
        		int itemId = Integer.parseInt(m.group(1));
        		items.add(itemId);
        	} catch (Exception e) {
				log.error(e.getMessage());
			}
        }
    	return items;
    }

	@Override
	public List<PlantillaModel> buscarPlantillas(int txId) {
		return plantillaMapper.buscarPlantillas(txId);
	}

	@Override
	public List<PlantillaPropuestaModel> buscarPlantilla(int txId) {
		return plantillaMapper.buscarPlantilla(txId);
	}
	
	

}
