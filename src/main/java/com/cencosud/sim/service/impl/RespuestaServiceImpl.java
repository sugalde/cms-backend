package com.cencosud.sim.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.mapper.RespuestaMapper;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.service.RespuestaService;
import com.cencosud.sim.util.Util;

@Service("RespuestaService")
public class RespuestaServiceImpl implements RespuestaService{
	static final Logger log = Logger.getLogger(FileUploadServiceImpl.class);
	
	@Autowired
	private RespuestaMapper respuestaMapper;

	@Override
	public void registrarRespuesta(List<RespuestaModel> respuestas, int recId, int empresa) {
		try {
			
			Util util = new Util();
			List<List<RespuestaModel>> respuestasArray = util.cortarArray(respuestas, 100);
			if(Constants.EMPRESA_CORREO_ELECTRONICO == empresa) {
				for (List<RespuestaModel> list : respuestasArray) {
					respuestaMapper.registrarRespuestaEmail(list);
				}
			}else {
				for (List<RespuestaModel> list : respuestasArray) {
					respuestaMapper.registrarRespuesta(list);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
	}

	@Override
	public List<RespuestaModel> obtenerRespuestasEmail(int recId) {
		List<RespuestaModel> respuesta = null;
		try {
			respuesta = respuestaMapper.obtenerRespuestasEmail(recId);
		} catch (Exception e) {
			log.error(e);
		}
		return respuesta;
	}

}
