package com.cencosud.sim.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.dto.Accion;
import com.cencosud.sim.mapper.TransaccionMapper;
import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.model.response.MessageResponse;
import com.cencosud.sim.service.TransaccionService;

@Service("TransaccionService")
public class TransaccionServiceImpl implements TransaccionService {
	static final Logger log = Logger.getLogger(TransaccionServiceImpl.class);
	
	@Autowired
	private TransaccionMapper transaccionMapper;

	@Override
	public TransaccionModel registrarTransaccion(CargaPcbsRequest cargaPcbsRequest) {
		TransaccionModel transaccion = new TransaccionModel();
		try {
			
			Date fechaActual = new Date();
			transaccion.setTxFchInicio(fechaActual);
			transaccion.setEmpId(Constants.EMPRESA_CARGA);
			transaccion.setTxUsuario(cargaPcbsRequest.getUsuario());
			transaccion.setEstId(cargaPcbsRequest.getEstado());
			transaccion.setTptxId(cargaPcbsRequest.getTptxId());
			if(cargaPcbsRequest.getTxId() > 0) {
				transaccion.setTxIdPadre(cargaPcbsRequest.getTxId());
			}
			if(cargaPcbsRequest.getTxFchInicio() != null && cargaPcbsRequest.getTxFchTermino() != null) {
				transaccion.setTxFchInicioCarga(cargaPcbsRequest.getTxFchInicio());
				transaccion.setTxFchTerminoCarga(cargaPcbsRequest.getTxFchTermino());
			}
			int respuesta = transaccionMapper.registrarTransaccion(transaccion);
			boolean registro = respuesta > 0;
			log.info(registro?"SE HA REGISTRADO LA TRANSACCION":"HA OCURRIDO UN ERROR AL REGISTRAR LA TRANSACCION");
			log.info(transaccion);
		} catch (Exception e) {
			log.error(e);
		}
		return transaccion;
	}

	@Override
	public int guardarTransaccion(TransaccionModel transaccionModel) {
		try {
			return transaccionMapper.guardarTransaccion(transaccionModel);
		} catch (Exception e) {
			log.error(e);
		}
		return 0;
	}

	@Override
	public List<CargasPcbsResponse> obtenerTransacciones() {
		List<CargasPcbsResponse> transacciones = transaccionMapper.obtenerTransacciones();
		
		for (CargasPcbsResponse cargasPcbs : transacciones) {
			List<Accion> acciones = new ArrayList<>();
			switch (cargasPcbs.getEstId()) {
			case 1:
				acciones.add(new Accion(Constants.BTN_VALIDAR));
			break;
			case 2:
				acciones.add(new Accion(Constants.BTN_PROCESAR));
			break;
			case 3:
				acciones.add(new Accion(Constants.BTN_RE_VALIDAR));
				acciones.add(new Accion(Constants.BTN_PROCESAR));
			break;
			case 4:
				acciones.add(new Accion(Constants.BTN_GENERAR_SALIDA));
			break;
			case 5:
				acciones.add(new Accion(Constants.BTN_RE_VALIDAR));
				acciones.add(new Accion(Constants.BTN_GENERAR_SALIDA));
			break;
			case 6:
				acciones.add(new Accion(Constants.BTN_DISTRIBUIR));
			break;
			
			
			default:
				break;
			}
			cargasPcbs.setAccion(acciones);
		}
		
		
		return transacciones;
	}

	@Override
	public CargasPcbsResponse obtenerTransaccion(int txId) {
		CargasPcbsResponse cargasPcbsResponse = new CargasPcbsResponse();
		try {
			cargasPcbsResponse = transaccionMapper.obtenerTransaccion(txId);
		} catch (Exception e) {
			log.error(e);
		}
		return cargasPcbsResponse;
	}

	@Override
	public TransaccionModel obtenerTransaccionPadre(int txId) {
		return transaccionMapper.obtenerTransaccionPadre(txId);
	}

	@Override
	public TransaccionModel obtenerUltimaTransaccion(int txId) {
		return transaccionMapper.obtenerUltimaTransaccion(txId);
	}

	@Override
	public MessageResponse obtenerMensaje(CargaPcbsRequest cargaPcbsRequest) {
		try {
			MessageResponse mensaje = transaccionMapper.obtenerMensaje(cargaPcbsRequest);
			String detalle = mensaje.getDetalle();
			if(detalle != null) {
				detalle = detalle.replaceAll("\\{\\{id\\}\\}", String.valueOf(cargaPcbsRequest.getTxId()));
				mensaje.setDetalle(detalle);
			}
			
			return mensaje;
		} catch (Exception e) {
			log.info(e);
		}
		return null;
	}

	@Override
	public void guardarRutaArchivo(int propuesta, String nombreArchivo, String ruta, int txId) {
		try {
			transaccionMapper.guardarRutaArchivo(propuesta,nombreArchivo,ruta,txId);
		} catch (Exception e) {
			log.error(e);
		}
		
	}
}
