package com.cencosud.sim.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import com.cencosud.sim.constants.Constants;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

public class ArchivoServidor {
	static final Logger log = Logger.getLogger(ArchivoServidor.class);
	
	/**
	 * 
	 * @param filename
	 * @return
	 */
	public String getStringHTML(String filename) {
        try {
        	return String.join("", getHTML(filename));
		} catch (Exception e) {
			log.error(e);
			return "";
		}
    }
	
	/**
	 * Obtiene el contenido de un archivo en un listado de String<br/>
	 * Se conecta por medio de smb a la carpeta compartida
	 * @param filename
	 * @return
	 */
	private List<String> getHTML(String filename) {
		SmbFile file = null;
		List<String> list = new ArrayList<>();
		byte[] buffer = new byte[1024];
		try {
			log.info("'"+Constants.SERVER+filename.replaceAll("\\/", "")+"'");
		    String url = Constants.SERVER+filename;
		    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, Constants.USERNAMESERVER, Constants.PASSWORD);
		    file = new SmbFile(url, auth);
		    try (SmbFileInputStream in = new SmbFileInputStream(file)) {
		        int bytesRead = 0;
		        do {
		            bytesRead = in.read(buffer);
		        } 
		        while (bytesRead > 0);
		    }
		} catch(Exception e) {
			log.error("ERROR RESPUESTAS 1");
		    log.info(e);
		}
		try {
			if(file != null && !file.exists()) {
				log.info("No Existe");
			}else {
				list = leerArchivoSmb(file);
			}
		} catch (SmbException e) {
			log.error(e);
		}
		
		return list;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	private List<String> leerArchivoSmb(SmbFile file) {
		List<String> list = new ArrayList<>();
		try (
				BufferedReader reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(file),Constants.ENCODING))
			) {
				list = new ArrayList<>();
		    	String line = reader.readLine();
			    while (line != null) {
			        line = reader.readLine();
			        if(line != null)
			        	list.add(line);
			    }
		}catch (Exception e) {
			log.error(e);
		}
		return list;
	}
	
	public void guardarArchivoSmb(File archivoExcel, String ruta) {
			
		try (
				SmbFileOutputStream sfos = new SmbFileOutputStream(autenticacion(ruta));
				InputStream in = new FileInputStream(archivoExcel);
			) {

	        // Copy the bits from instream to outstream
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = in.read(buf)) > 0) {
	        	sfos.write(buf, 0, len);
	        }
	        in.close();
	        sfos.close();
		} catch (IOException e) {
			log.error(e);
		}
	}
	
	public File archivoTemporal(MultipartFile file) {
		String[] fileNameSplit = file.getOriginalFilename().split("\\.");
		String extension = fileNameSplit[fileNameSplit.length -1];
		String filename = file.getOriginalFilename().replaceAll("."+extension, "");
		File temp = null;
		try {
			temp = File.createTempFile(filename+"-SIM-", "."+extension);
			log.info("ARCHIVO TEMPORAL CREADO : " + temp.getAbsolutePath());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(temp));
			stream.write(file.getBytes());
			stream.close();
		} catch (Exception e) {
			log.error(e);
		}
		return temp;
	}
	
	private SmbFile autenticacion(String ruta) {
		NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, Constants.USERNAMESERVER, Constants.PASSWORD);
		SmbFile sFile = null;
		try {
			sFile = new SmbFile(ruta, auth);
		} catch (MalformedURLException e1) {
			log.error(e1);
		}
		return sFile;
	}
	
	public File obtenerArchivoSmb(String ruta){
		String[] archivoDesc = ruta.split("\\.");
		String extension = archivoDesc[archivoDesc.length -1];
		File tempFile = null;
		try {
			tempFile = File.createTempFile("eeeeee-SIM-", "."+extension);
		} catch (IOException e1) {
			log.error(e1);
		}
		
		try (
				SmbFileInputStream sfos = new SmbFileInputStream(autenticacion(ruta));
				FileOutputStream fos = new FileOutputStream(tempFile);
			) {
				

		        byte[] buf = new byte[1024];
		        int len;
		        while ((len = sfos.read(buf)) > 0) {
		        	fos.write(buf, 0, len);
		        }
		        sfos.close();
		        fos.close();
		}catch (Exception e) {
			log.error(e);
		}
		return tempFile;
	}
	
}
