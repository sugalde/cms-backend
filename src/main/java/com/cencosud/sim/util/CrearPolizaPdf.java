package com.cencosud.sim.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.model.PlantillaPropuestaModel;
import com.cencosud.sim.service.OperacionService;

public class CrearPolizaPdf {
	static final Logger log = Logger.getLogger(CrearPolizaPdf.class);
	
	@Autowired
	@Qualifier("OperacionService")
	private OperacionService operacionService;

	/**
	 * Metodo sobrecargado de generacionPDF para la generacion de multiples documentos en uno solo
	 * para una sola propuesta, este crea archivos temporales que posteriormente seran eliminados,
	 * las plantillas seran concatenadas dadas por el orden natural (implementacion de la interface comparable)
	 * de la Clase {@link com.cencosud.sim.model.PlantillaPropuestaModel#compareTo(PlantillaPropuestaModel)} 
	 */
	public String generacionPdf(int idPropuesta, SortedSet<PlantillaPropuestaModel> setPlantillas, String directorioSalida) {

		try{
			final AtomicInteger counter = new AtomicInteger(0);
			String nombreDocFinal = "01-"+ idPropuesta;
			LinkedList<String> lstNomDocTemporales = new LinkedList<>();
			//Por cada plantilla genera un archivo temporal
			setPlantillas.stream()
			 			 .forEachOrdered(plantilla -> {
			
			 				String filename = plantilla.getPllaRuta();
			 				//Obtener plantilla
			 		        String[] tpl = {new ArchivoServidor().getStringHTML(filename)};
			 		        //Recorrer la plantilla en busca de elementos a reemplazar desde el model
			 		        plantilla.getModeloDatos()
			 		        		 .forEach((k, v) -> {
			 		        			tpl[0] = tpl[0].replaceAll("\\[" + k + "\\]", v==null?"":v.toString());  
			 		        		 });
			 		        StringBuilder nombreDocTemporal = new StringBuilder();
			 		        nombreDocTemporal.append("t");
			 		        nombreDocTemporal.append(counter.incrementAndGet());
			 		        nombreDocTemporal.append("-");
			 		        nombreDocTemporal.append(idPropuesta);
			 		        lstNomDocTemporales.add(nombreDocTemporal.toString());
			 		        wkHtmlPdf(tpl[0], directorioSalida+nombreDocTemporal);
			 		        
			 			 });
			//Pega todos los archivos temporales en uno solo, luego elimina dichos archivos
			return mergeTempPDF(directorioSalida, lstNomDocTemporales, nombreDocFinal);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
    
	/**
	 * Mediante la lista de nombre de archivos temporales existente, crea un solo documento PDF,
	 * posteriormente elimina dichos archivos
	 * El orden en que concatena es en base al orden de los objetos insertados en lstNomDocTemporales
	 * Retorna el nombre del archivo creado 
	 * @param directorioSalida
	 * @param lstNomDocTemporales
	 * @param nombreDocFinal
	 * @throws IOException
	 */
    private String mergeTempPDF(String directorioSalida, LinkedList<String> lstNomDocTemporales, String nombreDocFinal) throws IOException {

    	String extension = ".pdf";
		PDFMergerUtility ut = new PDFMergerUtility();
		List<InputStream> lstArchivoProp = new LinkedList<>();
		boolean merge = false;
    	try {
    		lstNomDocTemporales.stream().forEachOrdered(nomDocTemporal -> {
    			try {
    				lstArchivoProp.add(new FileInputStream(directorioSalida.concat(nomDocTemporal)
    																	   .concat(extension)));
    			} catch (FileNotFoundException e) {
    				log.error(ExceptionUtils.getStackTrace(e));
    			}
    		});
    		
    		ut.addSources(lstArchivoProp);
    		ut.setDestinationFileName(directorioSalida.concat(nombreDocFinal)
    												  .concat(extension));
    		//Ejecuta el merge de los archivos
    		ut.mergeDocuments(null);
    		merge = true;
	
    	} finally {
    		//Liberamos recursos temporales
    		lstArchivoProp.forEach(docTem -> {
    			try {
    				docTem.close();
    			} catch (IOException e) {
    				log.error(e);
    			}
    		});
		}
    	
		//Borramos recursos temporales
    	if(merge){
    		lstNomDocTemporales.parallelStream()
    						   .forEach(nomDocTemporal -> {
    							   
    			File file = new File(directorioSalida.concat(nomDocTemporal).concat(extension));
    			file.delete();
    			if(!file.exists()){
    				log.info("Archivo temporal borrado exitosamente: " + nomDocTemporal);
    			}
    		});	
    	}
    	return nombreDocFinal.concat(extension);
		
    }

    /**
     * Genera archivos pdf con el contenido html que se le entrega
     * @param contenido
     * @param ruta
     */
    private void wkHtmlPdf(String contenido, String ruta) {
    	
    	try (
    			PrintWriter writer = new PrintWriter(ruta+".html", Constants.ENCODING);
    		){
    		
            writer.println(contenido);
            writer.close();
            File archivoHtml = new File(ruta+".html");
            //Se define el archivo pdf final
            File archivoPdf=new File(ruta+".pdf");
            
            log.info(Constants.WKHTMLTOPDF + " --encoding "+Constants.ENCODING+" '"+ archivoHtml.getPath() +"' '"+archivoPdf.getPath()+"'");
            // Se ejecuta wkhtmltopdf 
            ProcessBuilder pb = new ProcessBuilder(Constants.WKHTMLTOPDF, 
			                    "--encoding",
			                    Constants.ENCODING,
			                    archivoHtml.getPath(), 
			                    archivoPdf.getPath());
			Process process = pb.start();
			pb.redirectErrorStream(true);
			BufferedReader errStreamReader = new BufferedReader(new  InputStreamReader(process.getErrorStream())); 
			//not "process.getInputStream()" 
			String line = null;
			while((line = errStreamReader.readLine()) != null){ 
				log.debug(line); 
			}
			            
            
            log.info(archivoHtml.exists()+" : "+archivoHtml.getAbsolutePath());
            log.info(archivoPdf.exists()+" : "+archivoPdf.getAbsolutePath());
        } catch (Exception ex) { 
        	log.error(ex); 
        }
    }
    

}
