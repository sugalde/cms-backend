package com.cencosud.sim.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;


public class ExcelUtil {

	static final Logger log = Logger.getLogger(ExcelUtil.class);
	
	public static void creaExcelResumen(Map<String, List<Map<String , Object>>> modeloResumen) throws IOException {
		
		
		modeloResumen.forEach((directorioSalida, lstDatosModelo) ->{
			
			String nombreEmpresa = Paths.get(directorioSalida)
					                    .getFileName()
					                    .toString()
					                    .trim()
					                    .replaceAll("\\.", "");
			
			try(HSSFWorkbook workbook = new HSSFWorkbook();
					FileOutputStream file = new FileOutputStream(directorioSalida
							                                     +"resumen_"
							                                     + nombreEmpresa
																 +".xls")){
					
					HSSFSheet sheet = workbook.createSheet();
					workbook.setSheetName(0, "Propuestas");
					
			        String[] headers = getHeaderResumen();
			        
			        
			        Object[][] data = new Object[lstDatosModelo.size()][21];
			        //llenamos datos
			        for(int x = 0; x < lstDatosModelo.size(); x++){
		                
			        	//Numero de registro resumen
			        	data[x][0] = x+1;
			        	data[x][1] = lstDatosModelo.get(x).get(Items.CODIGO_BARRA.getIdItem());
			        	data[x][2] = lstDatosModelo.get(x).get(Items.FOLIO.getIdItem());
			        	
			        	StringBuilder nombreCompleto = new StringBuilder();
			        	nombreCompleto.append(lstDatosModelo.get(x).get(Items.NOMBRE_CONTRATANTE.getIdItem()));
			        	nombreCompleto.append(StringUtils.SPACE);
			        	nombreCompleto.append(lstDatosModelo.get(x).get(Items.APELLIDO1_CONTRATANTE.getIdItem()));
			        	nombreCompleto.append(StringUtils.SPACE);
			        	nombreCompleto.append(lstDatosModelo.get(x).get(Items.APELLIDO2_CONTRATANTE.getIdItem()));
			        	
			        	data[x][3] = nombreCompleto.toString();
			        	data[x][4] = lstDatosModelo.get(x).get(Items.DIRECCION.getIdItem());
			        	data[x][5] = lstDatosModelo.get(x).get(Items.COMUNA.getIdItem());
			        	data[x][6] = lstDatosModelo.get(x).get(Items.CIUDAD.getIdItem());
			        	data[x][7] = lstDatosModelo.get(x).get(Items.TELEFONO.getIdItem());
			        	data[x][8] = lstDatosModelo.get(x).get(Items.PLAN.getIdItem());
			        	data[x][9] = null;
			        	data[x][10] = nombreEmpresa;
			        	
			        	data[x][11] = lstDatosModelo.get(x).get(Items.NUM_PROPUESTA.getIdItem());
			        	data[x][12] = null;
			        	data[x][13] = null;
			        	
			        	data[x][14] = null;
			        	data[x][15] = null;
			        	String rutCompleto = (String)lstDatosModelo.get(x).get(Items.RUT.getIdItem());
			        	if(rutCompleto != null){
			        		String[] rut = rutCompleto.split("-");
			        		if(rut != null && rut.length == 2){
				        		data[x][14] = rut[0];
					        	data[x][15] = rut[1];	
				        	}
			        	}
			        	
			        	data[x][16] = null;
			        	data[x][17] = lstDatosModelo.get(x).get(Items.EMAIL.getIdItem());
			        	data[x][18] = lstDatosModelo.get(x).get(Items.NOMBRE_ARCHIVO.getIdItem());
			        	data[x][19] = null;
			        	
			        	data[x][20] = null;
			         }
			        
			        CellStyle headerStyle = workbook.createCellStyle();
			        Font font = workbook.createFont();
			        font.setBold(true);
			        headerStyle.setFont(font);

			        CellStyle integerStyle = workbook.createCellStyle();
			        integerStyle.setDataFormat((short) 0);
			        

			        HSSFRow headerRow = sheet.createRow(0);
			        for (int i = 0; i < headers.length; ++i) {
			            String header = headers[i];
			            HSSFCell cell = headerRow.createCell(i);
			            cell.setCellStyle(headerStyle);
			            cell.setCellValue(header);
			        }

			        for (int i = 0; i < data.length; ++i) {
			            HSSFRow dataRow = sheet.createRow(i + 1);

			            Object[] d = data[i];
			            Integer idResumen = (Integer) d[0];
			            String codigoBarra = (String) d[1];
			            Integer folio = Integer.valueOf((String)d[2]);
			            String nombreContratante = (String) d[3];
			            
			            String direccion = (String) d[4];
			            String comuna = (String) d[5];
			            String ciudad = (String) d[6];
			            String telefono = (String) d[7];
			            String plan  = String.valueOf(d[8]);
			            
			            String empresaDespacho = (String) d[10];
			            Integer propuesta = (Integer) d[11];
			            
			            String rut1 = (String) d[14];
			            String rut2 = (String) d[15];
			            
			            String email = (String) d[17];
			            String nombreArchivo = (String) d[18];

			            //CREACION DE CELDAS
			            Cell cell0 = dataRow.createCell(0);
			            cell0.setCellStyle(integerStyle);
			            cell0.setCellValue(idResumen);
			            
			            dataRow.createCell(1).setCellValue(codigoBarra);
			            
			            Cell cell2 = dataRow.createCell(2);
			            cell2.setCellStyle(integerStyle);
			            cell2.setCellValue(folio);
			            
			            dataRow.createCell(3).setCellValue(nombreContratante);
			            
			            dataRow.createCell(4).setCellValue(direccion);
			            dataRow.createCell(5).setCellValue(comuna);
			            dataRow.createCell(6).setCellValue(ciudad);
			            dataRow.createCell(7).setCellValue(telefono);
			            dataRow.createCell(8).setCellValue(plan);
			            
			            dataRow.createCell(10).setCellValue(empresaDespacho);
			            
			            Cell cell11 = dataRow.createCell(11);
			            cell11.setCellStyle(integerStyle);
			            cell11.setCellValue(propuesta);
			            
			            dataRow.createCell(14).setCellValue(rut1);
			            dataRow.createCell(15).setCellValue(rut2);
			            dataRow.createCell(17).setCellValue(email);
			            dataRow.createCell(18).setCellValue(nombreArchivo);
			        }
			        
			        for (int i = 0; i < headers.length; ++i) {
			        	sheet.autoSizeColumn(i);	
			        }
			        workbook.write(file);
			        
				} catch (IOException e) {
					log.error(e);
					ExceptionUtils.getStackTrace(e);
				}
		});
		
		
		
		

	}
	
	private static String[] getHeaderResumen(){
		 return new String[]{
	        		"N°",
	        		"CODIGO BARRA",
	        		"OT",
	        		"NOMBRE",
	        		"DIRECCION",
	        		"COMUNA",
	        		"CIUDAD",
	        		"FONO CLIENTE",
	        		"PLAN",
	        		"PLAN DESCRIPCIÓN",
	        		"MEDIO DESPACHO",
	        		"N° PROPUESTA",
	        		"CIAS NOMBRE",
	        		"ASISTENCIA",
	        		"RUT",
	        		"DV",
	        		"RAMO DESCRIP",
	        		"EMAIL",
	        		"NOMBRE ARCHIVO",
	        		"TV",
	        		"CODIGO ASISTENCIA"
	            };
	}
	
	private ExcelUtil() {
		super();
	}

}
