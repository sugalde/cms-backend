package com.cencosud.sim.util;

public enum Items {

	NUM_PROPUESTA("NUMPROPUESTA"),
	NOMBRE_ARCHIVO("NOMARCHIVO"),
	PLAN("PLAN"),
	NOMBRE_CONTRATANTE("9233"),
	APELLIDO1_CONTRATANTE("9231"),
	APELLIDO2_CONTRATANTE("9232"),
	DIRECCION("9020"),
	COMUNA("9040"),
	CIUDAD("9050"),
	TELEFONO("9060"),
	EMAIL("9330"),
	FOLIO("9999"),
	CODIGO_BARRA("10030"),
	RUT("9220");
	
	private String idItem;
	
	private Items(String idItem){
		this.idItem = idItem;
	}
	
	public String getIdItem(){
		return idItem;
	}
	
}
