package com.cencosud.sim.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cencosud.sim.constants.Constants;


public class Util {
	static final Logger log = Logger.getLogger(Util.class);
	
	/**
	 * Verifica si un String puede ser parseado a Integer
	 * @param numero
	 * @return
	 */
	public boolean convetInt(String numero) {
		try {
			log.info("parseInt : "+Integer.parseInt(numero));
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Verifica si un String puede ser parseado a Date
	 * @param fechaValidar
	 * @param formato
	 * @return
	 */
	public boolean esFechaValida(String fechaValidar, String formato){

		if(fechaValidar == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		sdf.setLenient(false);
		try {
			sdf.parse(fechaValidar);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	public Map<Integer, String> stringMapIS(String str) {
		String strLimpio = str.replaceAll("\\{", "").replaceAll("\\}", "");
	    String[] tokens = strLimpio.split(", ");
	    Map<Integer, String> map = new HashMap<>();
	    for (int i=0; i<tokens.length; i++) {
	    	String[] data = tokens[i].split("=");
	    	map.put(Integer.parseInt(data[0]),data[1]);
	    }
	    return map;
	}
	
	/**
	 * corta una lista para los casos en que es dificil de procesar
	 * @param list
	 * @param L
	 * @return
	 */
	public <T> List<List<T>> cortarArray(List<T> list, final int l) {
	    List<List<T>> parts = new ArrayList<>();
	    final int cantidad = list.size();
	    for (int i = 0; i < cantidad; i += l) {
	        parts.add(new ArrayList<T>(
	            list.subList(i, Math.min(cantidad, i + l)))
	        );
	    }
	    return parts;
	}
	
	public String convertirFecha(String fecha) {
		SimpleDateFormat diaMesAnio = new SimpleDateFormat(Constants.FECHA_FRONT);
	    SimpleDateFormat mesDiaAnioGuion = new SimpleDateFormat(Constants.FECHA_BACK);
	    Date fechaGuion = new Date();
		try {
			fechaGuion = diaMesAnio.parse(fecha);
			log.info(mesDiaAnioGuion.format(fechaGuion));
			return mesDiaAnioGuion.format(fechaGuion);
		} catch (ParseException e1) {
			log.error(e1);
		}
		return fecha;
	}
	
}
