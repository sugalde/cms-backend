package com.cencosud.sim;

import org.junit.Test;

public class CalculosTest {
	
	@Test
	public void calcular() {
		try {
			System.out.println((int)25/	9);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
}
