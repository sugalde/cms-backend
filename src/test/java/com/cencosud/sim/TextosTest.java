package com.cencosud.sim;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.cencosud.sim.util.LeerArchivosExcel;

public class TextosTest {
	static final Logger log = Logger.getLogger(LeerArchivosExcel.class);
	
	@Test
	public void listas() {
	      // create a hash set
	      LinkedHashSet<Integer> hs = new LinkedHashSet<>();
	      
	      // add elements to the hash set
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      
	      hs.add(4);
	      hs.add(4);
	      hs.add(5);
	      hs.add(6);
	      hs.add(1);
	      hs.add(82);
	      hs.add(83);
	      hs.add(83);
	      hs.add(28);
	      hs.add(82);
	      hs.add(82);
	      hs.add(82);
	      hs.add(822);
	      hs.add(82);
	      hs.add(822);
	      hs.add(22);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      hs.add(8);
	      
	      
	      System.out.println(hs);
	   }
	
	public void probarTexto() {
		String data = "21/01/2015";
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			log.info(formatter.parse(data));
		} catch (ParseException e) {
			log.error(e);
		}
		
		
	}
	
	public boolean convetInt(String numero) {
		try {
			log.info("parseInt : "+Integer.parseInt(numero));
		} catch (NumberFormatException ne) {
			return false;
		}
		return true;
	}
	
	private String modifyDateLayout(String inputDate){
        try {
            //inputDate = "2010-01-04 01:32:27 UTC";
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").parse(inputDate);
            return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
        } catch (ParseException e) {
            log.error(e);
            return "15.01.2010";
        }
    }
	
	private boolean esFecha(double numericCellValue) {
		log.info(numericCellValue);
		String valor = String.valueOf(numericCellValue);
		if(".0".equals(valor.substring(valor.length() - 2))) {
			log.info("Fecha");
		}else if("0.".equals(valor.substring(0, 2))){
			log.info("hora");
		}
		log.info(valor);
		return false;
	}
	
	public boolean esFechaValida(String fechaValidar, String formato){

		if(fechaValidar == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		sdf.setLenient(false);
		try {
			sdf.parse(fechaValidar);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	public static Map<Integer, String> stringMapIS(String str) {
		String strLimpio = str.replaceAll("\\{", "").replaceAll("\\}", "");
	    String[] tokens = strLimpio.split(", ");
	    Map<Integer, String> map = new HashMap<>();
	    for (int i=0; i<tokens.length; i++) {
	    	String[] data = tokens[i].split("=");
	    	map.put(Integer.parseInt(data[0]),data[1]);
	    }
	    return map;
	}
	
	public static String obtenerValoresReferencia(String referencia) {
		String[] refDescompuesta = referencia.split("-");
		int pptaId = Integer.parseInt(refDescompuesta[1]);
		int txId = Integer.parseInt(refDescompuesta[2]);
		log.info(pptaId);
		log.info(txId);
		return null;
	}
}
